#!/bin/bash

_condor_local_dir=`condor_config_val LOCAL_DIR` || exit 5

exec "$@"
