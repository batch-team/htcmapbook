#!/bin/bash

set +x
set +e

/usr/bin/python3 /usr/local/bin/jupyter-labhub --ip=0.0.0.0 --port=8888 --allow-root

echo "Done... exiting"
