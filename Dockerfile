FROM jupyter/minimal-notebook:latest

LABEL maintainer="ben.dylan.jones@cern.ch"

ARG HTCONDOR_VERSION=8.9

# default to jupyter lab
ENV JUPYTER_ENABLE_LAB=1

USER root

# add HTCondor config
COPY etc/condor/condor_config.local /etc/condor/condor_config.local

# add an entrypoint that starts HTCondor
COPY entrypoint.sh /.entrypoint.sh
COPY start.sh /.start.sh

# install HTCondor, and a few other convenience tools
# also fix permissions on the files copied above
RUN apt-get update -y \
 && apt-get install -y gnupg vim less git man \
 && wget -qO - https://research.cs.wisc.edu/htcondor/ubuntu/HTCondor-Release.gpg.key | apt-key add - \
 && echo "deb  http://research.cs.wisc.edu/htcondor/ubuntu/${HTCONDOR_VERSION}/focal focal contrib" >> /etc/apt/sources.list \
 && apt-get update -y \
 && apt-get install -y htcondor \
 && apt-get clean -y \
 && rm -rf /var/lib/apt/lists/* \
 && chmod +x /.entrypoint.sh \
 && chmod +x /.start.sh \
 && fix-permissions ${HOME}

USER $NB_UID:$NB_GID

# install HTCondor Python bindings, HTChirp, and HTMap
RUN pip install --no-cache-dir htcondor htchirp htmap

# set up environment variable hooks for HTMap settings
# todo: once HTMap can decide the delivery method itself, remove it from here!
# todo: embed image name so HTMap can use the same image for exec
ENV HTMAP_DELIVERY_METHOD="assume"

# ENTRYPOINT ["tini", "-g", "--", "/.entrypoint.sh"]
# CMD ["start-notebook.sh"]
ENTRYPOINT /.start.sh
